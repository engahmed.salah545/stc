FROM selenium/standalone-chrome

WORKDIR /stc

COPY stc.jar /stc/
COPY stc-dependencies-jars /stc/

CMD ["java", "-cp", ".:/stc/*", "TestRunner"]