# Welcome to STC Project


## Project Features

- Build mangement with maven
- Selenium BDD with Cucumber
- POM design pattern
- TestNG
- CI with Jenkins
- Docker
- Reporting with extent report and cucumber report
