pipeline {
    agent any

    environment {
        GIT_URL = 'https://gitlab.com/engahmed.salah545/stc.git'
    }

    stages {
        stage('Clone Repository') {
            steps {
                checkout([$class: 'GitSCM', branches: [[name: 'main']], userRemoteConfigs: [[url: GIT_URL]]])
            }
        }

        stage('Build and Test') {
            steps {
                bat 'mvn clean test'
            }
        }
    }

    post {
        always {
			emailext subject: "Build Status: ${currentBuild.currentResult}",
                 body: "Build Status: ${currentBuild.currentResult}\nJob Name: ${env.JOB_NAME}\nBuild Number: #${env.BUILD_NUMBER}\nBuild URL: ${BUILD_URL}",
                 to: "engahmed.salah545@gmail.com"
        }
    }
}