import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = "src/test/java/tests",
        glue = {"tests"},
        tags ="@Bahrain or @KSA or @Kuwait",
        plugin = { "html:reports/cucumber-report.html", "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:" }

)

public class TestRunner extends AbstractTestNGCucumberTests {
}
