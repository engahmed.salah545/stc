package browser;

import driver.WebDriverSingleton;
import pages.BahrinChooseYourPlan;
import pages.KSAChooseYourPlan;
import pages.KuwaitChooseYourPlan;

import java.io.IOException;


public class Browser {
    private final WebDriverSingleton webDriverSingleton = WebDriverSingleton.getDriverSingleton();
    public static KSAChooseYourPlan ksaChooseYourPlan;
    public static BahrinChooseYourPlan bahrainChooseYourPlan;
    public static KuwaitChooseYourPlan kuwaitChooseYourPlan;



    public Browser() throws IOException {
        ksaChooseYourPlan = new KSAChooseYourPlan();
        bahrainChooseYourPlan= new BahrinChooseYourPlan();
        kuwaitChooseYourPlan=new KuwaitChooseYourPlan();
    }


}


