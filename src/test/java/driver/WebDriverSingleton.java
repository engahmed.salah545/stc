package driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import properties_reading.ConfigProperties;

import java.io.IOException;

public class WebDriverSingleton {
    private static WebDriver webDriver;
    private static WebDriverSingleton webDriverSingleton = null;
    private String driverType = "driverType";


    private WebDriverSingleton() throws IOException {
        setDriverCapabilities();
    }

    public static WebDriverSingleton getDriverSingleton() throws IOException {
        if (webDriverSingleton == null)
            webDriverSingleton = new WebDriverSingleton();
        return webDriverSingleton;
    }

    private void setDriverCapabilities() throws IOException {
        String browserType = "resources/config/configBrowser.properties";
        ConfigProperties configBrowser = new ConfigProperties(browserType);
        driverType = configBrowser.getProperty(driverType);

        switch (driverType) {
            case "Chrome":
                WebDriverManager.chromedriver().setup();
                ChromeOptions chromeCapabilities = new ChromeOptions();
                chromeCapabilities.addArguments("--start-maximized");
                webDriver = new ChromeDriver();
                break;

            case "Edge":
                EdgeOptions edgeCapabilities = new EdgeOptions();
                webDriver = new EdgeDriver(edgeCapabilities);
                webDriver.manage().window().maximize();
                break;
        }
        }


    public static WebDriver getWebDriver() {
        return webDriver;
    }

    public void navigateTo(String link) {
        webDriver.navigate().to(link);
    }

    public void resetCache() throws InterruptedException {
        webDriver.manage().deleteAllCookies();
        //TODO: remove thread.sleep
        Thread.sleep(7000);
    }

    public void maximizeWindow() {
        webDriver.manage().window().maximize();
    }

    public static void close() {
        webDriver.quit();
    }
}

