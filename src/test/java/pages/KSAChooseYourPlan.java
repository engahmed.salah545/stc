package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class KSAChooseYourPlan extends BasePage {
    private final By countryButton = By.id("country-btn");
    private final By ksaButton = By.id("sa");
    private final By ksaLiteCurrency = By.id("currency-lite");
    private final By ksaClassicCurrency = By.id("currency-classic");
    private final By ksaPremiumCurrency = By.id("currency-premium");


    public void clickOnCountryButtonInKSAPage() {
        findElement(countryButton).click();
    }

    public void clickOnKSAButton() {
        findElement(ksaButton).click();
    }

    public WebElement getKSALiteCurrency() {
        return findElement(ksaLiteCurrency);
    }

    public WebElement getKSAClassicCurrency() {
        return findElement(ksaClassicCurrency);
    }
    public WebElement getKSAPremiumCurrency() {
        return findElement(ksaPremiumCurrency);
    }

}
