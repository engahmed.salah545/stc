package pages;

import driver.WebDriverSingleton;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public abstract class BasePage {
    WebDriver chromeDriver = WebDriverSingleton.getWebDriver();
    WebDriver edgeDriver = WebDriverSingleton.getWebDriver();
    final int waitTime = 35;

    public BasePage(){
        PageFactory.initElements(new AjaxElementLocatorFactory(chromeDriver, waitTime), this);
    }

    public WebElement findElement(By locator){
        WebDriverWait wait = new WebDriverWait(WebDriverSingleton.getWebDriver(), Duration.ofSeconds(35));
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        return WebDriverSingleton.getWebDriver().findElement(locator) ;
    }
}
