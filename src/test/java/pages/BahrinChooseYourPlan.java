package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class BahrinChooseYourPlan extends BasePage {
    private final By countryButton = By.id("country-btn");
    private final By bahrainButton = By.id("bh");
    private final By bahrainLiteCurrency = By.id("currency-lite");
    private final By bahrainClassicCurrency = By.id("currency-classic");
    private final By bahrainPremiumCurrency = By.id("currency-premium");

    public void clickOnCountryButtonInBahrainPage() {
        findElement(countryButton).click();
    }

    public void clickOnBahrainButton() {
        findElement(bahrainButton).click();
    }

    public WebElement getBahrainLiteCurrency() {
        return findElement(bahrainLiteCurrency);
    }

    public WebElement getBahrainClassicCurrency() {
        return findElement(bahrainClassicCurrency);
    }
    public WebElement getBahrainPremiumCurrency() {
        return findElement(bahrainPremiumCurrency);
    }

}
