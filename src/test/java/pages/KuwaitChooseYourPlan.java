package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class KuwaitChooseYourPlan extends BasePage{

    private final By countryButton = By.id("country-btn");
    private final By ksaButton = By.id("kw");
    private final By kuwaitLiteCurrency = By.id("currency-lite");
    private final By kuwaitClassicCurrency = By.id("currency-classic");
    private final By kuwaitPremiumCurrency = By.id("currency-premium");


    public void clickOnCountryButtonInKuwaitPage() {
        findElement(countryButton).click();
    }

    public void clickOnKuwaitButton() {
        findElement(ksaButton).click();
    }

    public WebElement getKuwaitLiteCurrency() {
        return findElement(kuwaitLiteCurrency);
    }

    public WebElement getKuwaitClassicCurrency() {
        return findElement(kuwaitClassicCurrency);
    }
    public WebElement getKuwaitPremiumCurrency() {
        return findElement(kuwaitPremiumCurrency);
    }


}
