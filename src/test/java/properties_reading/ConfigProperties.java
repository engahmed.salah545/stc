package properties_reading;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigProperties {

    private static final String STCTestingEnvironmentConfigPath = System.getProperty("user.dir") + "/resources/config/testing-environment-config.properties";


    private Properties properties;
    private final String configFilePath;

    public ConfigProperties(String configFilePath) throws IOException {
        this.configFilePath = configFilePath;
        setConfigProperties();
    }

    private void setConfigProperties() throws IOException {
        properties = new Properties();
        properties.load(new FileInputStream(configFilePath));
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    public static Properties setTestingEnvironmentProperties() throws IOException {
        Properties configProperties = new Properties();
        FileInputStream inputStream = new FileInputStream(STCTestingEnvironmentConfigPath);
        configProperties.load(inputStream);
        return configProperties;
    }

}
