package tests;


import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter;
import driver.WebDriverSingleton;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import properties_reading.ConfigProperties;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import java.io.IOException;

public class HooksHandler extends BaseTest {
    String STCTestingLink;

    public HooksHandler() throws IOException {
    }

    @Before
    public void setUpSTC() throws InterruptedException, IOException {
        driver = WebDriverSingleton.getDriverSingleton();
        driver.resetCache();
        driver.maximizeWindow();
        properties = ConfigProperties.setTestingEnvironmentProperties();
        STCTestingLink = properties.getProperty("STCTestingLink");
        driver.navigateTo(STCTestingLink);

    }
    @AfterStep
    public void AfterStep(Scenario scenario) throws IOException, InterruptedException
    {
        if(scenario.isFailed())
        {
            ExtentCucumberAdapter.getCurrentStep().log(Status.FAIL, MediaEntityBuilder.createScreenCaptureFromBase64String(getBase64Screenshot()).build());
        }
    }

    public String getBase64Screenshot()
    {
        return ((TakesScreenshot) WebDriverSingleton.getWebDriver()).getScreenshotAs(OutputType.BASE64);
    }

    @After
    public void tearDown() throws InterruptedException {
        driver.resetCache();
        closeWebDriverAfterAllTestsHook();
    }

    private void closeWebDriverAfterAllTestsHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            WebDriverSingleton.close();
        }));
    }

}