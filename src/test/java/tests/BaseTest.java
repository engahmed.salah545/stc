package tests;

import browser.Browser;
import driver.WebDriverSingleton;

import java.io.IOException;
import java.util.Properties;

public class BaseTest {
    protected WebDriverSingleton driver;
    protected static Browser browser;

    static {
        try {
            browser = new Browser();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected static Properties properties;


    public BaseTest() throws IOException {
        driver = WebDriverSingleton.getDriverSingleton();
    }
}
