package tests.countries.bahrain;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import tests.BaseTest;

import java.io.IOException;

public class BahrainStepdefs extends BaseTest {

    public BahrainStepdefs() throws IOException {
    }

    @When("User change the country to bahrain")
    public static void userChangeTheCountryToBahrain() {
        browser.bahrainChooseYourPlan.clickOnBahrainButton();
    }

    @Then("the lite currency will be BHD {string}")
    public static void theLiteCurrencyWillBeBHD(String bahrainCurency) {
        Assert.assertEquals(browser.bahrainChooseYourPlan.getBahrainLiteCurrency().getText(), bahrainCurency);

    }

    @Then("the classic currency will be BHD {string}")
    public static void theClassicCurrencyWillBeBHD(String bahrainCurency) {
        Assert.assertEquals(browser.bahrainChooseYourPlan.getBahrainClassicCurrency().getText(), bahrainCurency);

    }

    @Then("the premium currency will be BHD {string}")
    public static void thePremiumCurrencyWillBeBHD(String bahrainCurency) {
        Assert.assertEquals(browser.bahrainChooseYourPlan.getBahrainPremiumCurrency().getText(), bahrainCurency);

    }

}

