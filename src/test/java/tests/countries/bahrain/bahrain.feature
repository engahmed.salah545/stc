Feature: bahrain
  @Bahrain
  Scenario Outline: check that the currency in lite section in bahrain
    Given  User click on country button
    When User change the country to bahrain
    Then the lite currency will be BHD "<currency>"


    Examples:
      | currency   |
      | 2 BHD/month|

#    Test Screenshot in Failure
  @Bahrain
  Scenario Outline: check that the currency in classic section in bahrain
    Given  User click on country button
    When User change the country to bahrain
    Then the classic currency will be BHD "<currency>"


    Examples:
      | currency   |
      | 5 BHD/month|
      | 3 BHD/month|

  @Bahrain
  Scenario Outline: check that the currency in premium section in bahrain
    Given User click on country button
    When User change the country to bahrain
    Then the premium currency will be BHD "<currency>"


    Examples:
      | currency    |
      | 6 BHD/month |