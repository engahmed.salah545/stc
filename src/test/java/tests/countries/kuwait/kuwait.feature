Feature: kuwait
  @Kuwait
  Scenario Outline: check that the currency in lite section in kuwait
    Given  User click on country button
    When User change the country to kuwait
    Then the lite currency will be KWD "<currency>"


    Examples:
      | currency     |
      | 1.2 KWD/month|

  @Kuwait
  Scenario Outline: check that the currency in classic section in kuwait
    Given  User click on country button
    When User change the country to kuwait
    Then the classic currency will be KWD "<currency>"


    Examples:
      | currency     |
      | 2.5 KWD/month|

  @Kuwait
  Scenario Outline: check that the currency in premium section in kuwait
    Given  User click on country button
    When User change the country to kuwait
    Then the premium currency will be KWD "<currency>"


    Examples:
      | currency     |
      | 4.8 KWD/month|