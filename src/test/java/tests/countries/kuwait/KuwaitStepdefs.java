package tests.countries.kuwait;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import tests.BaseTest;

import java.io.IOException;

public class KuwaitStepdefs extends BaseTest {

    public KuwaitStepdefs() throws IOException {
    }

    @When("User change the country to kuwait")
    public static void userChangeTheCountryToKSA() {
        browser.kuwaitChooseYourPlan.clickOnKuwaitButton();

    }
    @Then("the lite currency will be KWD {string}")
    public static void theLiteCurrencyWillBeKWD(String kuwaitCurency) {
        Assert.assertEquals(browser.kuwaitChooseYourPlan.getKuwaitLiteCurrency().getText(), kuwaitCurency);

    }

    @Then("the classic currency will be KWD {string}")
    public static void theClassicCurrencyWillBeKWD(String kuwaitCurency) {
        Assert.assertEquals(browser.kuwaitChooseYourPlan.getKuwaitClassicCurrency().getText(), kuwaitCurency);

    }

    @Then("the premium currency will be KWD {string}")
    public static void thePremiumCurrencyWillBeKWD(String kuwaitCurency) {
        Assert.assertEquals(browser.kuwaitChooseYourPlan.getKuwaitPremiumCurrency().getText(), kuwaitCurency);

    }

}
