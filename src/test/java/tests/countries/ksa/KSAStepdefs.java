package tests.countries.ksa;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import tests.BaseTest;

import java.io.IOException;

public class KSAStepdefs extends BaseTest {

    public KSAStepdefs() throws IOException {
    }

    @Given("User click on country button")
    public static void userClickOnCountryButton() {
        browser.ksaChooseYourPlan.clickOnCountryButtonInKSAPage();

    }

    @When("User change the country to KSA")
    public static void userChangeTheCountryToKSA() {
        browser.ksaChooseYourPlan.clickOnKSAButton();

    }

    @Then("the lite currency will be SAR {string}")
    public static void theLiteCurrencyWillBeSAR(String ksaCurency) {
        Assert.assertEquals(browser.ksaChooseYourPlan.getKSALiteCurrency().getText(),ksaCurency);

    }

    @Then("the classic currency will be SAR {string}")
    public static void theClassicCurrencyWillBeSAR(String ksaCurency) {
        Assert.assertEquals(browser.ksaChooseYourPlan.getKSAClassicCurrency().getText(),ksaCurency);

    }

    @Then("the premium currency will be SAR {string}")
    public static void thePremiumCurrencyWillBeSAR(String ksaCurency) {
        Assert.assertEquals(browser.ksaChooseYourPlan.getKSAPremiumCurrency().getText(),ksaCurency);

    }
}
