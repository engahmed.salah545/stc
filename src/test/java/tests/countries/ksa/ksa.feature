Feature: ksa
  @KSA
  Scenario Outline: check that the currency in lite section in ksa
    Given  User click on country button
    When User change the country to KSA
    Then the lite currency will be SAR "<currency>"


    Examples:
      | currency    |
      | 15 SAR/month|

  @KSA
  Scenario Outline: check that the currency in classic section in ksa
    Given  User click on country button
    When User change the country to KSA
    Then the classic currency will be SAR "<currency>"


    Examples:
      | currency    |
      | 25 SAR/month|

  @KSA
  Scenario Outline: check that the currency in premium section in ksa
    Given  User click on country button
    When User change the country to KSA
    Then the premium currency will be SAR "<currency>"


    Examples:
      | currency    |
      | 60 SAR/month|